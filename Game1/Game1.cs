using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Input.Touch;
using System.Collections.Generic;

namespace Game1
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class Game1 : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        Texture2D background, cursorLoc,gameLogo;
        Vector2 touchPos;
        TouchCollection touchInput;

        float playerRelativeSize = 0.095f;     // player's relative size (percentage) compared to the width of the screen
        int playerSize;

        SpriteFont testFont;
        SpriteFont testFont2;
        SpriteFont scoreFontBig;
        SpriteFont overheatFont;
        float warningRatio;
        int screenWidth, screenHeight;
        string resoString, objCount;

        Player player;
        PlayerControl playerControl;
        GameProperty gameProperty;
        string playerHealthText,playerChargeText,playerScoreText;
        Joystick playerJoystick, target;

        GUI_main generalGUI;

        ObjectGenerator objectGenerator;
        Background bg;

        List<GameObject> gameObjects;

        float bgSpeed = 1;

        BtnReset btnReset;
        // Temp
        int highScore;
        bool isPaused;
        float restTimeMax = 3000;
        float currRestTime;

        float instTimeMax = 5000;
        float currInstTime;
        Texture2D instTex;
        
        int logoSize;


        public Game1(int screenWidth, int screenHeight)
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";

            graphics.IsFullScreen = true;
            graphics.PreferredBackBufferWidth = 1080;
            graphics.PreferredBackBufferHeight = 1920;
            graphics.SupportedOrientations = DisplayOrientation.Portrait;

            this.screenWidth = screenWidth;
            this.screenHeight = screenHeight;

            

            
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here
            //background = Content.Load<Texture2D>("bg");
            instTex = Content.Load<Texture2D>("instruction");
            currInstTime = 0;
            resoString = "Device width: " + screenWidth + "px, " + screenHeight + "px";
            gameLogo = Content.Load<Texture2D>("overheatLogo");
            logoSize = (int)(screenWidth * 0.5);

            testFont = Content.Load<SpriteFont>("fontScore");
            testFont2 = Content.Load<SpriteFont>("fontMiniTitle");
            scoreFontBig = Content.Load<SpriteFont>("fontBigScore");
            overheatFont = Content.Load<SpriteFont>("fontOverheatWarning");
            Vector2 warningSize = overheatFont.MeasureString("OVERHEAT");
            warningRatio = screenWidth / warningSize.X;
            
            playerSize = (int)(screenWidth * playerRelativeSize);

            player = new Player(playerSize, playerSize, new Vector2(screenWidth / 2, (screenHeight - (5 * playerSize))));
            playerJoystick = new Joystick(playerSize, playerSize, new Vector2(screenWidth / 2, (screenHeight - (3 * playerSize))));
            target = new Joystick((int)(playerSize * 2.0), (int)(playerSize * 3.3), new Vector2(screenWidth / 2, (screenHeight - (1.5f * playerSize))));

            gameProperty = new GameProperty(Content, screenWidth, screenHeight,player);

            playerControl = new PlayerControl(player, playerJoystick, target, gameProperty);

            gameObjects = new List<GameObject>();
            gameObjects.Add(player);

            playerHealthText = player.health + "%";
            playerChargeText = player.getCharge() + "%";
            playerScoreText = "" + gameProperty.getScore();

            objectGenerator = new ObjectGenerator(gameProperty, gameObjects, 64);

            touchInput = TouchPanel.GetState();

            objCount = "Objects: " + gameObjects.Count;

            generalGUI = new GUI_main(Vector2.Zero, screenWidth, screenHeight,gameProperty);

            bg = new Background(gameProperty, bgSpeed);
            // Temp
            highScore = 0;
            isPaused = true;
            player.kill();
            currRestTime = 0;
            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            // TODO: use this.Content to load your game content here
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed)
                Exit();



            // TODO: Add your update logic here
            touchInput = TouchPanel.GetState();
            

            if(gameProperty.getPlayerHealth() <= 0)
            {
                currInstTime = 0;
                currRestTime += gameTime.ElapsedGameTime.Milliseconds;
                if(currRestTime >= restTimeMax)
                {
                    currRestTime = restTimeMax;
                }

                
                if (touchInput.Count > 0 && currRestTime >= restTimeMax)
                {
                    if(touchInput[0].State == TouchLocationState.Released)
                    {
                        currRestTime = 0;
                        playerSize = (int)(screenWidth * playerRelativeSize);

                        player = new Player(playerSize, playerSize, new Vector2(screenWidth / 2, (screenHeight - (3 * playerSize))));
                        playerJoystick = new Joystick((int)(playerSize * 1.3), (int)(playerSize * 1.3), new Vector2(screenWidth / 2, (screenHeight - (1.5f * playerSize))));

                        target = new Joystick((int)(playerSize * 1.3), (int)(playerSize * 1.3), new Vector2(screenWidth / 2, (screenHeight - (1.5f * playerSize))));

                        gameProperty = new GameProperty(Content, screenWidth, screenHeight, player);

                        playerControl = new PlayerControl(player, playerJoystick, target, gameProperty);

                        gameObjects = new List<GameObject>();
                        gameObjects.Add(player);
                        gameObjects.Add(playerJoystick);
                        gameObjects.Add(target);

                        playerHealthText = player.health + "%";
                        playerChargeText = player.getCharge() + "%";
                        playerScoreText = "" + gameProperty.getScore();

                        objectGenerator = new ObjectGenerator(gameProperty, gameObjects, 64);
                        bg = new Background(gameProperty, bgSpeed);

                        generalGUI = new GUI_main(Vector2.Zero, screenWidth, screenHeight, gameProperty);

                        // Temp

                        isPaused = true;
                    }
                }

            }
            else
            {
                currInstTime += gameTime.ElapsedGameTime.Milliseconds;
                if(currInstTime >= instTimeMax)
                {
                    currInstTime = instTimeMax;
                }

                bg.Update(gameTime);
                playerControl.Update(gameTime);

                objectGenerator.Update(gameTime);

                foreach (var g in gameObjects)
                {
                    g.Update(gameTime, gameObjects);
                }


                // Remove dead objects
                int i = 0;
                while (i < gameObjects.Count)
                {
                    GameObject g = gameObjects[i];

                    if (!g.isAlive && !g.isPlayer)
                    {
                        gameObjects.RemoveAt(i);
                    }
                    else
                    {
                        i++;
                    }
                }
                objCount = "Objects: " + gameObjects.Count;


                playerHealthText = player.health + "%";
                playerChargeText = player.getCharge() + "%";
                playerScoreText = "" + gameProperty.getScore();

                if (gameProperty.getScore() > highScore)
                {
                    highScore = gameProperty.getScore();
                }

                generalGUI.Update(gameTime);
            }
            

            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Black);

            // TODO: Add your drawing code here
            spriteBatch.Begin();

            if (gameProperty.getPlayerHealth() <= 0)
            {
                GraphicsDevice.Clear(Color.White);

                float logoPosX = (screenWidth / 2) - (logoSize / 2);
                spriteBatch.Draw(gameLogo, new Rectangle((int)logoPosX, 0, logoSize, logoSize), Color.White);

                Vector2 s1a = testFont2.MeasureString("Best Score");
                Vector2 s1b = scoreFontBig.MeasureString("" + highScore);
                float s1a_newPos = (screenWidth / 2) - (s1a.X / 2);
                float s1b_newPos = (screenWidth / 2) - (s1b.X / 2);

                spriteBatch.DrawString(testFont2, "Best Score", new Vector2(s1a_newPos, logoSize + 10), Color.Gray);
                spriteBatch.DrawString(scoreFontBig, "" + highScore, new Vector2(s1b_newPos, logoSize + 25), Color.Black);

                Vector2 s2a = testFont2.MeasureString("Current Score");
                Vector2 s2b = scoreFontBig.MeasureString("" + gameProperty.getScore());
                float s2a_newPos = (screenWidth / 2) - (s2a.X / 2);
                float s2b_newPos = (screenWidth / 2) - (s2b.X / 2);

                spriteBatch.DrawString(testFont2, "Current Score", new Vector2(s2a_newPos, logoSize + 150), Color.Gray);
                spriteBatch.DrawString(scoreFontBig, "" + gameProperty.getScore(), new Vector2(s2b_newPos, logoSize + 165), Color.Black);

                Vector2 s3a = testFont.MeasureString("Tap screen to play");
                float s3a_newPos = (screenWidth / 2) - (s3a.X / 2);

                if (currRestTime >= restTimeMax)
                {
                    spriteBatch.DrawString(testFont, "Tap screen to play", new Vector2(s3a_newPos, logoSize + 310), Color.DarkGray);
                }
            }
            else
            {
                bg.Draw(spriteBatch);

                if(currInstTime< instTimeMax)
                {
                    spriteBatch.Draw(instTex, new Rectangle(0, (int)((screenHeight / 2) - (screenHeight / 2)), screenWidth, screenWidth), Color.White);
                }


                // Warning message, if any
                if (player.isOverheating)
                {
                    Color warningColor = Color.DarkSalmon;
                    warningColor.A = 16;

                    spriteBatch.DrawString(overheatFont, "OVERHEAT", new Vector2(0, screenHeight / 2), warningColor, 0f, Vector2.Zero, warningRatio, SpriteEffects.None, 0f);
                }

                foreach (var g in gameObjects)
                {
                    if (!g.isPlayer)
                    {
                        g.Draw(spriteBatch);
                    }
                }

                player.Draw(spriteBatch);

                float halfwayPoint = 0;
                /*
                spriteBatch.DrawString(testFont2, "Health", new Vector2(10, halfwayPoint + 10), Color.White);
                spriteBatch.DrawString(testFont, playerHealthText, new Vector2(10, halfwayPoint + 20), Color.White);
                */

                /*
                Vector2 chargeTitleSize = testFont2.MeasureString("Heat");
                spriteBatch.DrawString(testFont2, "Heat", new Vector2((screenWidth - chargeTitleSize.X), halfwayPoint + 10), Color.White);
                Vector2 chargeAmountSize = testFont.MeasureString(playerChargeText);
                spriteBatch.DrawString(testFont, playerChargeText, new Vector2((screenWidth - chargeAmountSize.X), halfwayPoint + 20), Color.White);
                */


                Vector2 scoreTitleSize = testFont2.MeasureString("Current Score");
                Vector2 scoreMainSize = scoreFontBig.MeasureString(playerScoreText);

                float scoreTitleLoc = (screenWidth / 2) - (scoreTitleSize.X / 2);
                float scoreMainLoc = (screenWidth / 2) - (scoreMainSize.X / 2);

                spriteBatch.DrawString(testFont2, "Current Score", new Vector2(scoreTitleLoc, 10), Color.White);
                spriteBatch.DrawString(scoreFontBig, playerScoreText, new Vector2(scoreMainLoc, 25), Color.White);

                generalGUI.Draw(spriteBatch);
            }

            spriteBatch.End();

            base.Draw(gameTime);
        }
    }
}
