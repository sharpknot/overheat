﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace Game1
{
    class PlayerControl
    {
        
        Player player;
        GameProperty gameProperty;
        Texture2D playerTexture, joystickTexture, targetTexture, dragmeTexture;
        float speed = 0.3f;    // player move speed relative to it's own size
        float currChargeCounter;
        float maxTimePerCharge = 300;        // max miliseconds per charge change
        Joystick playerJoystick, target;
        Boolean prevIsJoystickPressed, prevIsPressed;
        float tolerenceDistance;

        Vector2 joyStickLocation;
        Boolean dragmeOn;
        public Boolean isDragging;

        public PlayerControl(Player player, Joystick playerJoystick, Joystick target, GameProperty gameProperty)
        {
            this.player = player;
            this.gameProperty = gameProperty;
            this.playerJoystick = playerJoystick;
            this.target = target;

            dragmeOn = true;

            dragmeTexture = gameProperty.getContent().Load<Texture2D>("dragme");
            player.dragmeTip = dragmeTexture;

            playerTexture = gameProperty.getContent().Load<Texture2D>("playerTexture");
            joystickTexture = gameProperty.getContent().Load<Texture2D>("joystickTexture");
            targetTexture = gameProperty.getContent().Load<Texture2D>("targetControl");
            player.setTexture(playerTexture);
            playerJoystick.setTexture(joystickTexture);
            target.setTexture(targetTexture);
            currChargeCounter = 0;
            prevIsJoystickPressed = false;
            prevIsPressed = false;
            isDragging = false;

            player.joysticLoc = playerJoystick.getPosition();
            tolerenceDistance = playerJoystick.getWidth() * 0.5f;

            player.addExplodeTexture(gameProperty.getContent().Load<Texture2D>("expl1"));
            player.addExplodeTexture(gameProperty.getContent().Load<Texture2D>("expl2"));
            player.addExplodeTexture(gameProperty.getContent().Load<Texture2D>("expl3"));
            player.addExplodeTexture(gameProperty.getContent().Load<Texture2D>("expl4"));
            player.addExplodeTexture(gameProperty.getContent().Load<Texture2D>("expl5"));
        }

        public void Update(GameTime gameTime)
        {
            float tempY = player.getPosition().Y;
            float tempYj = playerJoystick.getPosition().Y;
            float tempX = player.getPosition().X;
            PlayerInput input = new PlayerInput();

            Vector2 tempVector, tempVectorJ;

            currChargeCounter += gameTime.ElapsedGameTime.Milliseconds;

            //check if player is continously pressing the screen
            if(input.hasTouch() && !prevIsPressed) // if player touches the screen for the first time, and previously was not pressed
            {
                // first time input
                prevIsPressed = true;

                // get whether the first touch is in the joystick
                if (playerJoystick.isPressed(input.getTouchPosition())) // player is touching the joystick first
                {
                    prevIsJoystickPressed = true;
                }
                else // player is not touching the joystick
                {
                    prevIsJoystickPressed = false;
                }
            }
            else if(input.hasTouch() && prevIsPressed)  //if player is touching the screen for the second time (previously was pressed)
            {
                // if player was initially touching the joystick (previously)
                if (prevIsJoystickPressed)
                {
                    if (Vector2.Distance(playerJoystick.getPosition(), input.getTouchPosition()) > tolerenceDistance)
                    {
                        isDragging = true;  // dragging state on
                        player.showDragMe = false;
                    }
                }
                else if(!prevIsJoystickPressed && playerJoystick.isPressed(input.getTouchPosition()))   // if player did not initially touch the joystick, but now it is
                {
                    prevIsJoystickPressed = true;
                }
                
            }
            else // Player releases the touch
            {
                prevIsPressed = false;
                prevIsJoystickPressed = false;
                isDragging = false;
            }


            if (isDragging)
            {
                tempX = input.getTouchPosition().X;

                // add charge every maxTimePerCharge milisecond
                if (currChargeCounter> maxTimePerCharge)
                {
                    player.addCharge(1);
                }
            }
            else
            {
                // remove charge every maxTimePerCharge milisecond
                if (currChargeCounter > maxTimePerCharge)
                {
                    player.removeCharge(2);
                }

            }

            tempVector = new Vector2(tempX, tempY);
            tempVectorJ = new Vector2(tempX, tempY);

            playerMoveTo(tempVector);
            joystickMoveTo(tempVectorJ);

            if (isDragging)
            {
                target.setPosition(tempVectorJ);
            }
            else
            {
                target.setPosition(playerJoystick.getPosition());
            }

        }

        void playerMoveTo(Vector2 newPos)
        {
            Vector2 velocity;

            if (Vector2.Distance(newPos,player.getPosition()) >= (speed * player.getWidth()))
            {
                velocity = Vector2.Subtract(newPos, player.getPosition());
                velocity.Normalize();
                velocity = Vector2.Multiply(velocity, speed * player.getWidth());

                player.setPosition(player.getPosition() + velocity);
            }
            else
            {
                player.setPosition(newPos);
            }
        }

        void joystickMoveTo(Vector2 newPos)
        {
            Vector2 velocity;

            if (Vector2.Distance(newPos, playerJoystick.getPosition()) >= (speed * player.getWidth()))
            {
                velocity = Vector2.Subtract(newPos, playerJoystick.getPosition());
                velocity.Normalize();
                velocity = Vector2.Multiply(velocity, speed * player.getWidth());

                playerJoystick.setPosition(playerJoystick.getPosition() + velocity);
            }
            else
            {
                playerJoystick.setPosition(newPos);
            }
        }

    }
}