﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Game1
{
    class BtnReset : Button
    {
        Texture2D textureDefault, texturePressed;
        SpriteFont buttonFont;

        public BtnReset(int width, int height, Vector2 position, GameProperty gameProperty, string text) : base(width, height, position, gameProperty, text)
        {
            textureDefault = gameProperty.getContent().Load<Texture2D>("green_button13");
            texturePressed = gameProperty.getContent().Load<Texture2D>("green_button00");

            buttonFont = gameProperty.getContent().Load<SpriteFont>("fontButton");

        }

        public override void Update(GameTime gameTime, List<GameObject> gameObjects)
        {
            base.Update(gameTime, gameObjects);
        }
        public override void Draw(SpriteBatch spriteBatch)
        {
            Vector2 textSize = buttonFont.MeasureString(text);
            float textRelPosX = position.X  - (textSize.X / 2);
            float textRelPosY = position.Y - (textSize.Y/2);

            if (!isDefault())
            {
                spriteBatch.Draw(textureDefault, getHitbox(), Color.White);
                spriteBatch.DrawString(buttonFont, text, new Vector2(textRelPosX, textRelPosY), Color.Black);
            }
            else
            {
                spriteBatch.Draw(texturePressed, getHitbox(), Color.White);
                spriteBatch.DrawString(buttonFont, text, new Vector2(textRelPosX, textRelPosY), Color.Red);
            }

            base.Draw(spriteBatch);
        }
    }
}