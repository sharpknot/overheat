﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Game1
{
    class Button : GameObject
    {
        GameProperty gameProperty;
        string currentState,prevState;
        protected string text;
        PlayerInput input;

        public Button(int width, int height, Vector2 position, GameProperty gameProperty, string text) : base(width, height, position)
        {
            this.gameProperty = gameProperty;
            this.text = text;
            input = new PlayerInput();
            currentState = "default";
            prevState = "default";
        }

        public override void Update(GameTime gameTime, List<GameObject> gameObjects)
        {
            if (input.isTouchPressing())
            {
                if (isInsideHitbox(input.getTouchPosition()))
                {
                    currentState = "pressed";
                }

            }
            else
            {
                if(prevState == "pressed")
                {
                    currentState = "released";
                }
                else
                {
                    currentState = "default";
                }
            }


            prevState = currentState;
            base.Update(gameTime, gameObjects);
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            base.Draw(spriteBatch);
        }

        Boolean isInsideHitbox(Vector2 positionTouch)
        {
            return this.getHitbox().Contains(positionTouch.X, positionTouch.Y);

        }

        public Boolean isPressed()
        {
            return currentState == "pressed";
        }

        public Boolean isReleased()
        {
            return currentState == "released";
        }

        public Boolean isDefault()
        {
            return currentState == "default";
        }

    }
}