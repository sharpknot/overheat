﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Game1
{
    class ObjectGenerator
    {
        GameProperty gameProperty;
        List<GameObject> gameObjects;
        int maxSize;

        Random rand;

        float currTime;
        float maxPrepTime = 6000;      // 3000 miliseconds prep time

        float maxRockDistance;    // Max distance between rocks (vertically)
        float currentRockDistance;
        int randomRockChance = 3;       // rock would spawn in a 1 in X chance

        float maxScoreDistance;
        float currentScoreDistance;
        int randomScoreChance = 10;     // Scores will spawn in a 1 in X chance

        int scoreLowChance;        // Low score in a X of 100 chance
        int scoreMidChance = 30;        // Low score in a X of 100 chance
        int scoreHighChance = 10;        // Low score in a X of 100 chance

        Texture2D scoreLowTexture, scoreMidTexture, scoreHighTexture;
        int scoreLowAmount = 5;
        int scoreMidAmount = 10;
        int scoreHighAmount = 20;

        List<Texture2D> rockTextures;

        public ObjectGenerator(GameProperty gameProperty, List<GameObject> gameObjects, int maxSize)
        {
            this.gameObjects = gameObjects;
            this.gameProperty = gameProperty;
            this.maxSize = maxSize;

            currTime = 0;

            rockTextures = new List<Texture2D>();

            maxScoreDistance = maxSize * 2;
            maxRockDistance = maxSize;
            rand = new Random();

            scoreLowChance = 100 - scoreMidChance - scoreHighChance;

            scoreLowTexture = gameProperty.getContent().Load<Texture2D>("star_bronze");
            scoreMidTexture = gameProperty.getContent().Load<Texture2D>("star_silver");
            scoreHighTexture = gameProperty.getContent().Load<Texture2D>("star_gold");

            for(int i = 1; i <= 8; i++)
            {
                string fileName = "rock" + i;

                Texture2D tempRockTex = gameProperty.getContent().Load<Texture2D>(fileName);
                rockTextures.Add(tempRockTex);
            }
            
            currentScoreDistance = 0;
            currentRockDistance = 0;
        }

        public void Update(GameTime gameTime)
        {

            currTime += gameTime.ElapsedGameTime.Milliseconds;

            if(currTime < maxPrepTime)
            {
                currentRockDistance = 0;
                currentScoreDistance = 0;
            }
            else
            {
                currTime = maxPrepTime;
            }

            // Generate empty "starting regions"
            int regionAmount = gameProperty.getScreenWidth() / maxSize;
            List<Vector2> possibleRegions = new List<Vector2>();
            regionAmount++;

            for(int i = 0; i < (regionAmount-1); i++)
            {
                Vector2 tempVector = new Vector2((i * maxSize) + maxSize, -maxSize);
                possibleRegions.Add(tempVector);
            }

            #region Rock Spawning
            currentRockDistance += gameProperty.getGlobalSpeed();

            if(currentRockDistance >= maxRockDistance)
            {
                int rockChance = rand.Next(0,randomRockChance);

                if (rockChance == 0)
                {

                    // Spawn a single rock (random size)
                    int halfSize = maxSize / 2;
                    int tempSize = rand.Next(halfSize, maxSize);
                    int randPos = rand.Next(0, possibleRegions.Count -1);

                    Vector2 choosenPos = possibleRegions[randPos];

                    int rockIndex = rand.Next(rockTextures.Count);
                    Texture2D tempRockTexture = rockTextures[rockIndex];


                    ObjRock tempRock = new ObjRock(tempSize, tempSize, choosenPos, gameProperty, tempRockTexture);
                    gameObjects.Add(tempRock);

                    possibleRegions.RemoveAt(randPos);
                }

                currentRockDistance = 0;
            }
            #endregion


            #region Score Spawning
            currentScoreDistance += gameProperty.getGlobalSpeed();

            if(currentScoreDistance >= maxScoreDistance)
            {
                int scoreChance = rand.Next(0, randomScoreChance);

                if(scoreChance == 0)
                {
                    int halfSize = maxSize / 2;
                    int randPos = rand.Next(0, possibleRegions.Count - 1);
                    Vector2 choosenPos = possibleRegions[randPos];

                    int whichScore = rand.Next(0, 100);
                    ObjScore tempScore;
                    
                    if(whichScore >= 0 && whichScore < scoreHighChance)
                    {
                        // highest score
                        tempScore = new ObjScore(maxSize, maxSize, choosenPos, gameProperty, scoreHighTexture, scoreHighAmount);

                    }
                    else if(whichScore >= scoreHighChance && whichScore < (scoreHighChance + scoreMidChance))
                    {
                        // medium score
                        tempScore = new ObjScore(maxSize, maxSize, choosenPos, gameProperty, scoreMidTexture, scoreMidAmount);

                    }
                    else
                    {
                        // lowest score;
                        tempScore = new ObjScore(maxSize, maxSize, choosenPos, gameProperty, scoreLowTexture, scoreLowAmount);

                    }

                    gameObjects.Add(tempScore);

                    possibleRegions.RemoveAt(randPos);

                }

                currentScoreDistance = 0;

            }

            #endregion
        }

        public void Reset()
        {
            currTime = 0;
            currentScoreDistance = 0;
            currentRockDistance = 0;
        }


    }
}