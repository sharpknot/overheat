﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Game1
{
    class Player : GameObject
    {
        Texture2D playerTexture, currentExplodeTexture;
        int charge,maxCharge;
        public Boolean isOverheating, showDragMe;
        public Vector2 joysticLoc;


        float overheatTimerMax = 300;   // Overheat damage applied every X seconds
        float currentOverheatTimer;

        List<Texture2D> explodeTextures;

        float currentHitTime;
        float maxHitTime = 100;
        Random rand;

        public Texture2D dragmeTip;


        public Player(int width, int height, Vector2 position) : base(width, height, position)
        {
            this.isPlayer = true;
            this.health = 100;      // set player health to 100
            charge = 0;
            maxCharge = 100;
            currentOverheatTimer = 0;
            isOverheating = false;

            explodeTextures = new List<Texture2D>();
            currentHitTime = maxHitTime;
            rand = new Random();

            showDragMe = true;

        }

        public override void Update(GameTime gameTime, List<GameObject> gameObjects)
        {
            // Damages player if overheat
            currentOverheatTimer += gameTime.ElapsedGameTime.Milliseconds;
            currentHitTime += gameTime.ElapsedGameTime.Milliseconds;

            if (charge >= maxCharge)
            {
                isOverheating = true;
                if (currentOverheatTimer >= overheatTimerMax)
                {
                    Damage(1);
                    currentOverheatTimer = 0;
                }
            }
            else
            {
                isOverheating = false;
                currentOverheatTimer = 0;
            }

            if(currentHitTime>= maxHitTime)
            {
                currentHitTime = maxHitTime;

                if (explodeTextures.Count > 0)
                {
                    int index = rand.Next(explodeTextures.Count - 1);
                    currentExplodeTexture = explodeTextures[index];
                }
            }


            base.Update(gameTime, gameObjects);
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            base.Draw(spriteBatch);
            
            // Draw Player
            spriteBatch.Draw(texture, new Rectangle((int)position.X - (width / 2), (int)position.Y - (height / 2), width, height), Color.White);

            if(currentHitTime < maxHitTime && explodeTextures.Count > 0)
            {
                spriteBatch.Draw(currentExplodeTexture, new Rectangle((int)(position.X - ((width*1.5) / 2)), (int)(position.Y - ((height*1.5) / 2)), (int)(width * 1.5), (int)(height * 1.5)), Color.White);
            }

            if(showDragMe && joysticLoc != null && dragmeTip != null)
            {
                spriteBatch.Draw(dragmeTip, new Rectangle((int)joysticLoc.X + (width / 2), (int)position.Y - height, 4*width, 2*height), Color.White);
            }

        }

        public override void setTexture(Texture2D playerTexture)
        {
            this.texture = playerTexture;
            this.playerTexture = this.texture;
        }

        public override Texture2D getTexture()
        {
            return playerTexture;
        }

        public void setPosition(Vector2 position)
        {
            this.position = position;
        }

        public override void Damage(int damageAmount)
        {
            health = health - damageAmount;

            // Temp solution
            if (health < 0)
            {
                health = 0;
            }

            currentHitTime = 0;
            base.Damage(damageAmount);
        }

        public void addCharge(int chargeAmount)
        {
            charge = charge + chargeAmount;

            if(charge >= maxCharge)
            {
                charge = maxCharge;
            }
        }


        public int getMaxCharge()
        {
            return maxCharge;
        }

        public int getCharge()
        {
            return charge;
        }

        public void removeCharge(int chargeAmount)
        {
            charge = charge - chargeAmount;

            if (charge < 0)
            {
                charge = 0;
            }
        }

        public void kill()
        {
            health = 0;
        }

        public void reset()
        {
            this.health = 100;
            charge = 0;
            currentOverheatTimer = 0;
        }

        public void addExplodeTexture(Texture2D explTexture)
        {
            explodeTextures.Add(explTexture);
        }

    }
}