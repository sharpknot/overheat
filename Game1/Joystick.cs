﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Game1
{
    class Joystick : GameObject
    {
        public Joystick(int width, int height, Vector2 position) : base(width, height, position)
        {


        }

        public override void Update(GameTime gameTime, List<GameObject> gameObjects)
        {
            base.Update(gameTime, gameObjects);


        }

        public Boolean isPressed(Vector2 inputLocation)
        {
            return getHitbox().Contains(inputLocation);
        }

        public void setPosition(Vector2 position)
        {
            this.position = position;
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            base.Draw(spriteBatch);

            Color outColor = Color.White;
            outColor.A = 128;

            /*
            // Draw JoystickButton
            spriteBatch.Draw(texture, new Rectangle((int)position.X - (width / 2), (int)position.Y - (height / 2), width, height), outColor);
            */

        }
    }
}