﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace Game1
{
    class Background
    {
        Texture2D bgTexture, bgTexture2;
        float speed;

        GameProperty gameProperty;
        Vector2 velocity, velocity1;
        int size, size2;
        float currTime;
        float maxTime = 10;

        List<BGObj> bgList;
        List<BGObj> bgList2;

        public Background(GameProperty gameProperty, float speed)
        {
            this.gameProperty = gameProperty;
            this.speed = speed;
            bgTexture = gameProperty.getContent().Load<Texture2D>("bg");
            bgTexture2 = gameProperty.getContent().Load<Texture2D>("bg2");
            size = bgTexture.Height;
            size2 = bgTexture2.Height;
            bgList = new List<BGObj>();
            bgList2 = new List<BGObj>();

            velocity = new Vector2(0, speed);
            velocity1 = new Vector2(0, speed*2);

            BGObj bg1 = new BGObj(Vector2.Zero, size);
            BGObj bg2 = new BGObj(new Vector2(0, -size), size);
            BGObj bg3 = new BGObj(new Vector2(0, size), size);

            bgList.Add(bg1);
            bgList.Add(bg2);
            bgList.Add(bg3);

            BGObj bg1a = new BGObj(Vector2.Zero, size2);
            BGObj bg2a = new BGObj(new Vector2(0, -size2), size2);
            BGObj bg3a = new BGObj(new Vector2(0, size2), size2);

            bgList2.Add(bg1a);
            bgList2.Add(bg2a);
            bgList2.Add(bg3a);

            currTime = 0;
        }

        public void Update(GameTime gameTime)
        {
            currTime += gameTime.ElapsedGameTime.Milliseconds;

            if(currTime >= maxTime)
            {
                currTime = 0;

                foreach(BGObj b in bgList)
                {
                    b.move(velocity);
                }

                foreach (BGObj b in bgList2)
                {
                    b.move(velocity1);
                }

            }

            int i = 0;

            while(i < bgList.Count)
            {
                if (bgList[i].getPosition().Y >= (2*size))
                {
                    bgList.RemoveAt(i);

                    // create new one behind
                    BGObj tempBG = new BGObj(new Vector2(0, -size), size);
                    bgList.Add(tempBG);
                }
                else
                {
                    i++;
                }
            }


            i = 0;
            while (i < bgList2.Count)
            {
                if (bgList2[i].getPosition().Y >= (2 * size2))
                {
                    bgList2.RemoveAt(i);

                    // create new one behind
                    BGObj tempBG = new BGObj(new Vector2(0, -size2), size2);
                    bgList2.Add(tempBG);
                }
                else
                {
                    i++;
                }
            }
        }

        public void Draw(SpriteBatch spritebatch)
        {
            foreach(BGObj b in bgList)
            {
                spritebatch.Draw(bgTexture, b.getPosition(), Color.White);
            }

            foreach (BGObj b in bgList2)
            {
                spritebatch.Draw(bgTexture2, b.getPosition(), Color.White);
            }

        }


    }

    class BGObj
    {
        Vector2 position;
        int size;

        public BGObj(Vector2 position, int size)
        {
            this.position = position;
            this.size = size;
        }

        public void move(Vector2 velocity)
        {
            position = Vector2.Add(position, velocity);
            
        }

        public Vector2 getPosition()
        {
            return position;
        }
    }
}